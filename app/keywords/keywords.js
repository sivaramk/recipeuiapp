'use strict';

angular.module('myApp.keywords', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/keywords', {
    templateUrl: 'keywords/keywords.html',
    controller: 'keywordsCtrl'
  });
}])

.controller('keywordsCtrl', ['$scope','$location','$http','$window','ENV', function($scope, $location, $http, $window, ENV) {
    
    $scope.redirectToLogout = function(){
        sessionStorage.clear();  
        $location.path("login");
    }

    function init(){
        if(!sessionStorage.getItem('id')) {
            toastMessage('Unable to fetch list of keywords.', true);        
            $scope.redirectToLogout();
        } else {
            $http.get(ENV+'/api/generic/KeywordGroup/'+sessionStorage.getItem('id')).success( function(data){
                $scope.keywords = data;
                $scope.displayedKeywordsData = angular.copy($scope.keywords);       
            }, function(err){
                toastMessage('Unable to fetch list of keywords.', true);
            })
        }
    }

    init();
   

    function toastMessage(msg, type) {
        var x = document.getElementById("snackbar");
        $scope.serverMessage = msg;
        x.className = "show";
        x.style.background = type ? 'green' : 'red';
        setTimeout(function(){ 
            x.className = x.className.replace("show", ""); 
        }, 3000);
    }

    $scope.redirectToRecipes = function(){
        $location.path("home");
    }

    $scope.closeDialog = function (){
        $scope.keywordId = "";
        $scope.keywordName = "";
        $scope.keywordValue = "";
        init();
    }

    $scope.createKeyword = function(){
        var data = {
            "UserId": sessionStorage.getItem('id'),
            "Name": $scope.keywordName,
            "Value": $scope.keywordValue
        };
        $http.post(ENV+'/api/generic/KeywordGroup', data)
        .success(function(){
            toastMessage('Keyword added successfully.', true);
            $scope.closeDialog();
            $("#createKeyword").modal('hide');  
        })
        .error(function(){
            toastMessage('Unable to add keyword.', false);         
        });
    }

    $scope.editKeyword = function(row){
        $scope.keywordId = row._id;
        $scope.keywordName = row.Name;
        $scope.keywordValue = row.Value;
    }

    $scope.editSaveKeyword = function(){
        var data = {
            Name: $scope.keywordName,
            Value: $scope.keywordValue
        };
        $http.put(ENV+'/api/generic/KeywordGroup/'+$scope.keywordId, data)
        .success(function(){
            toastMessage('Keyword updated successfully.', true);
            $scope.closeDialog();
            $("#editKeyword").modal('hide');            
        })
        .error(function(){
            toastMessage('Unable to update keyword.', false);         
        });
    }

    $scope.deleteKeywordConfirmation = function(row){
        $scope.keywordId = row._id;
        $scope.keywordName = row.Name;
        $scope.keywordValue = row.Value;
    }

    $scope.deleteKeyword = function(row){
        $http.delete(ENV+'/api/generic/KeywordGroup/'+$scope.keywordId)
        .success(function(){
            toastMessage('Keyword deleted successfully.', true);
            $scope.closeDialog();
            $("#deleteKeyword").modal('hide');              
        })
        .error(function(){
            toastMessage('Unable to delete keyword.', false);         
        });
    }

}]);