'use strict';

angular.module('myApp.signup', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/signup', {
    templateUrl: 'signup/signup.html',
    controller: 'signUpCtrl'
  });
}])

.controller('signUpCtrl', ['$scope', '$location','$http','ENV', function($scope, $location, $http, ENV) {

  $scope.signup = function(){
    var payload = {
          "Username" :$scope.username,
          "Password" : $scope.password,
          "EmailAddress":$scope.emailAddress
    }
    $http.post(ENV+'/api/users', payload).success(function (data) {
        sessionStorage.setItem('id', data.Id);
        $location.path("home");

    }).error(function (err) {
      toastMessage("Unable to signup, please try again later.", false);
		});
  };

  function toastMessage(msg, type) {
      var x = document.getElementById("snackbar");
      $scope.serverMessage = msg;
      x.className = "show";
      x.style.background = type ? 'green' : 'red';
      setTimeout(function(){ 
          x.className = x.className.replace("show", ""); 
          // x.style.background = '';
      }, 3000);
  }

  $scope.redirectToLogin = function(){
    $location.path("login");
  }

}]);