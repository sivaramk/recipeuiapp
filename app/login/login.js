'use strict';

angular.module('myApp.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'login/login.html',
    controller: 'loginCtrl'
  });
}])

.controller('loginCtrl', ['$scope','$location','$http','$window','ENV', function($scope, $location, $http, $window, ENV) {
  $scope.progress = false;
  $scope.login = function(){ 
    $scope.progress = true;
    $http.get(ENV+'/api/users/'+$scope.username).success(function (data) {
      if(data){
        if(data.Password === $scope.password){
          sessionStorage.setItem('id', data._id);
          $location.path("home");
        } else {
          toastMessage("Incorrect Username or Password.", false);
        }
      } else {
        toastMessage("Incorrect Username or Password.", false);
      }
      $scope.progress = false;
    }).error(function (err) {
      $scope.progress = false;
      toastMessage("Unable to authenticate, please try again later.", false);
		});
    
  };

  function toastMessage(msg, type) {
      var x = document.getElementById("snackbar");
      $scope.serverMessage = msg;
      x.className = "show";
      x.style.background = type ? 'green' : 'red';
      setTimeout(function(){ 
          x.className = x.className.replace("show", ""); 
          // x.style.background = '';
      }, 3000);
  }

  $scope.redirectToSignup = function(){
    $location.path("signup");
  }
}]);