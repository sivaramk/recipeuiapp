'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'homeCtrl'
  });
}])

.controller('homeCtrl', ['$scope','$http','$window','$location','ENV',function($scope, $http, $window, $location, ENV) {

    $scope.redirectToLogout = function(){
        sessionStorage.clear();  
        $location.path("login");
    }


    function init(){
        if(!sessionStorage.getItem('id')) {
            toastMessage('Unable to fetch list of recipes.', false);
            $scope.redirectToLogout();
        } else {
            $http.get(ENV+'/api/users/'+sessionStorage.getItem('id')+'/recipes').success( function(data){
                $scope.smartTableData = data;
            }, function(err){
                toastMessage('Unable to fetch list of recipes.', false);
            })
        }
    }

    init();
    

    $scope.displayedData = angular.copy($scope.smartTableData);

    $scope.redirectToKeywords = function(){
        $location.path("keywords"); 
    }

    $scope.closeDialog = function (){
        $scope.fileFound = false;
        $scope.keyword = "";
        init();
    }

    $scope.downloadDocument = function(recipeAttachmentId, isTransformedRecipe){
        $http({
            url: ENV+'/api/users/'+sessionStorage.getItem('id')+'/recipes/Download/' + recipeAttachmentId +'?isTransformedRecipe=' + isTransformedRecipe,
            type: 'GET',
            headers: {
                "Content-type": "application/json; charset=utf-8"
            },
            responseType: 'blob'
        })
        .success(function(response, status, headers){
            var data = response,
                contentDispositionHeader = headers('Content-Disposition'),
                url = $window.URL || $window.webkitURL;
            $scope.fileName = contentDispositionHeader && contentDispositionHeader.replace("attachment; filename*=utf-8''","");
            $scope.fileName = decodeURIComponent($scope.fileName);
            var blob = new Blob([data], {type: 'application/octet-stream'});
            
            $scope.fileUrl = url.createObjectURL(blob);
            setTimeout(function(){
                document.getElementById('download-row-content').click();
            }, 0);
            toastMessage('Document downloaded successfully.', true);
        })
        .error(function(){
            toastMessage('Unable to download document.', false);
        });
    }


    function toastMessage(msg, type) {
        var x = document.getElementById("snackbar");
        $scope.serverMessage = msg;
        x.className = "show";
        x.style.background = type ? 'green' : 'red';
        setTimeout(function(){ 
            x.className = x.className.replace("show", ""); 
            // x.style.background = '';
        }, 3000);
    }

    function uploadFileToUrl(){
        if($scope.myFile && $scope.myFile.name){
            var fd = new FormData();
            fd.append('file', $scope.myFile);
            $http.post(ENV+'/api/users/'+sessionStorage.getItem('id')+'/recipes/upload?KeywordGroupId='+$scope.keyword._id, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
            })
            .success(function(){
                toastMessage($scope.myFile.name + ' added successfully.', true);
                $scope.closeDialog();
                $("#uploadRecipe").modal('hide');               
            })
            .error(function(){
                toastMessage('Unable to add ' + $scope.myFile.name, false);
                        
            });
        } else {
            toastMessage("Please attach a file.", false);            
        }
        
    }

    $scope.uploadDocument = function(){
        uploadFileToUrl();
    }

    $scope.removeDoc = function(){
        $scope.fileFound = false;
        $scope.myFile = {};
    }

    $scope.addDocument = function(){
        if($scope.myFile && $scope.myFile.name){
            $scope.fileFound = true;
        } else {
            $scope.fileFound = false;
        }
    }

    $scope.$watch("myFile", function(newValue, oldValue) {
        $scope.addDocument();
    });
    

    $scope.openingUploadDialog = function(){
        $http.get(ENV+'/api/generic/KeywordGroup/'+sessionStorage.getItem('id')).success( function(data){
            $scope.keywordsList = data;
        }, function(err){
            toastMessage('Unable to fetch list of keywords.', true);
        })
    }


}]);